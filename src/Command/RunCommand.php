<?php

namespace Nullsju\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use Nullsju\Prerender\ProcessManager;

class RunCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('prerender:run')
            ->setDescription('Process entries in the queue');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->isRunning()) {
            try {
                $prerender = new \Nullsju\Prerender();
                $processManager = $prerender->container->get('process_manager');
                $processManager->run();
            } catch (\Exception $e) {}
        } else {
            $output->writeln("Another process is already running, to terminate it, disable it in crontab and run: `ps aux | grep prerender:run`, kill it with `kill -9 PID`");
        }
    }

    public function isRunning()
    {
        exec("ps -eo args", $output, $result);

        $processCount = 0;
        foreach ($output as $command) {
            if (strpos($command, $this->getName()) !== false) {
                $processCount++;
            }
        }

        return $processCount > 1;
    }
}