<?php

namespace Nullsju\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class WorkerCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('prerender:process')
            ->setDescription('Process entries in the queue')
            ->addOption('id', 'i', InputOption::VALUE_REQUIRED, 'Id to queued item');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $prerender = new \Nullsju\Prerender();
            $queue = $prerender->container->get('queue');
            $render = $prerender->container->get('render');

            $item = $queue->get($input->getOption('id'));

            if ($item) {
                $queue->take($item['id']);
                $success = $render->run($item['url'], $item['path']);

                if ($success) {
                    $queue->done($item['id']);
                } else {
                    $queue->error($item['id'], "Something went wrong with rendering of the url, check Render.log");
                }
            }
        } catch (\Exception $error) {

            if ($error instanceof \Nullsju\Exceptions\RenderLimitException) {
                // Re add item to queue, since the limit for the prerender is reached for this hour
                $queue->add($item['url']);
                return 2;
            } elseif ($error instanceof \Nullsju\Exceptions\RenderErrorException) {
                return 3;
            } else {
                return 4;
            }

            $queue->error($item['id'], $error->getMessage());
        }
    }
}