<?php

namespace Nullsju;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;

class Prerender
{
    public $container;

    public function __construct()
    {
        $this->container = new ContainerBuilder();
        $this->registerServices()->registerConfiguration();
    }

    private function registerServices()
    {
        $loader = new YamlFileLoader($this->container, new FileLocator(__DIR__ . '/Resources/config'));
        $loader->load('services.yml');
        return $this;
    }

    public function registerConfiguration()
    {
        $configFolder = __dir__ . "/../../../../app/config";
        $configFolderPath = realpath($configFolder);
        $configFile = realpath($configFolder . "/prerender.yml");

        // For development
        // $configFolder = __dir__ . "/Resources/config";
        // $configFolderPath = realpath($configFolder);
        // $configFile = realpath($configFolder . "/config.yml");

        if ($configFile) {
            $loader = new YamlFileLoader($this->container, new FileLocator($configFolder));
            $loader->load('prerender.yml');
            // For development
            // $loader->load('config.yml');
        } else {
            throw new \Exception("Could not find config file, please make sure its located here: {$configFolderPath}/prerender.yml");
        }
        return $this;
    }
}
