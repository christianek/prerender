<?php

namespace Nullsju\Prerender;

class Queue
{
    private $logger;

    private $database;

    private $queue;

    public function __construct(Logger $logger, Database $database)
    {
        $this->logger = $logger->get('Queue');
        $this->database = $database;
    }

    public function add($url)
    {
        try {
            $this->database->table('queue')->insert([
                'url' => $url,
                'status' => 0,
                'queued' => time(),
                'started' => null,
                'finished' => null,
                'error' => ''
            ]);
            $this->logger->info("Added new url to queue: " . $url);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }

    public function get($id)
    {
        try {
            return $this->database->table('queue')->find($id);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            return null;
        }
    }

    public function getQueued()
    {
        try {
            return $this->database->table('queue')->where(['status' => 0])->all();
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            return [];
        }
    }

    public function getHalted()
    {
        try {
            return $this->database->table('queue')->where(["finished" => null])->all();
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            return [];
        }
    }

    public function enqueue($id)
    {
        try {
            $item = $this->database->table('queue')->find($id);

            $item['started'] = time();
            $item['status'] = 1;

            $this->database->table('queue')->update($id, $item);
            $this->logger->info("A entry has been queued for processing");
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }

    public function take($id)
    {
        try {
            $item = $this->database->table('queue')->find($id);

            $item['started'] = time();
            $item['status'] = 2;

            $this->database->table('queue')->update($id, $item);
            $this->logger->info("A entry is being processed");
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }

    public function done($id)
    {
        try {
            $item = $this->database->table('queue')->find($id);

            $item['finished'] = time();
            $item['status'] = 3;

            $this->database->table('queue')->update($id, $item);
            $this->logger->info("A entry has been processed");
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }

    public function error($id, $error)
    {
        try {
            $item = $this->database->table('queue')->find($id);

            $item['finished'] = time();
            $item['status'] = 4;
            $item['error'] = $error;

            $this->database->table('queue')->update($id, $item);
            $this->logger->info("A entry failed to be processed");
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }
}
