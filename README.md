# Prerender library

This library uses https://www.prerender.cloud to generate urls to static html files.

## Install

### composer.json
Minimal composer.json file:

```
{
    "name": "some/project",
    "require": {
        "07media/prerender": "dev-master"
    },
    "minimum-stability": "dev",
    "prefer-stable": true,
    "config": {
        "process-timeout": 3000,
        "bin-dir": "bin",
        "secure-http": false
    },
    "extra": {
        "incenteev-parameters": [
            {
                "file": "app/config/prerender.yml",
                "dist-file": "vendor/07media/prerender/src/Resources/config/config.yml"
            }
        ]
    },
    "scripts": {
        "post-install-cmd": [
            "Incenteev\\ParameterHandler\\ScriptHandler::buildParameters"
        ]
    },
    "repositories": [
        {
            "type": "git",
            "url": "https://o7media@bitbucket.org/07media/prerender.git"
        }
    ]
}
```
### Access to library

You need to give yourself permission to install the package on Bitbucket, this can be accomplished in 2 ways:

 1. By adding your ssh-key to the Bitbucket repo (`https://bitbucket.org/07media/prerender`) as a Access key or add a ssh-key to a user with access to the repo.
 2. By adding your Bitbucket credentials to `~/.composer/auth.json`

#### auth.json example

```
{
    "http-basic": {
        "bitbucket.org": {
            "username": "google@07.no",
            "password": "OUR_STANDARD_PASSWORD"
        }
    }
}
```

### Composer install

Run: `composer install`

For development, run: `composer run-script post-install-cmd`

For production, create config-file: `app/config/prerender.yml`

#### prerender.yml example

```
parameters:
    root_dir: /var/www/backend
    frontends:
        www.example1.com: /var/www/frontend1
        www.example2.com: /var/www/frontend2
    prerender_url: "https://service.prerender.cloud"
    prerender_api_key: "apikey"
    queue_database_path: "/absolute/path/to/prerender/db"
```

For capistrano deployment, this needs to be in `linked_files` in `deploy.rb`

### Permissions

Make sure these folders are writeable by server-user (apache|www-data):

 1. ROOTDIR/var/logs
 2. ROOTDIR/prerender-db (from prerender.yml example: queue_database_path)

`chown -R www-data:www-data var/ prerender-db/`

### Crontab

Add this to crontab: `crontab -e`

`*/10 * * * * /absolute/path/to/bin/prerender prerender:run`

This command listen for queue entries and generate static files

### Database

This library is using FlatDB (file-database), but its no longer available on github.

The documentation is instead moved to Database.md file in this repo and file moved to src/Prerender/Database

## Usage

Basic queue usage:

```
require __dir__ . "/vendor/autoload.php";

$prerender = new \Nullsju\Prerender();
$queue = $prerender->container->get('queue');
$queue->add("http://example.com/test");
```

Run this command to process queue: `bin/prerender prerender:run`

Basic render usage:

```
require __dir__ . "/vendor/autoload.php";

$prerender = new \Nullsju\Prerender();
$render = $prerender->container->get('render');
$render->render("http://example.com/test");
```

Normal usage:

Create a function to add url to queue

test.php
```
require __dir__ . "/vendor/autoload.php";

$prerender = new \Nullsju\Prerender();
$queue = $prerender->container->get('queue');
$queue->add("http://example.com/test");
```

Make sure `bin/prerender prerender:run` is running.

Run php script: `php test.php`

Or you could use the add command: `bin/prerender prerender:add --url=http://example.com/test`

## Queue

To display the queue, run this command: `bin/prerender prerender:display:queue`

Use `watch` to listen on the queue, ex: `watch -n 1 bin/prerender prerender:display:queue`

To watch a list of queued urls: `watch -n 1 bin/prerender prerender:display:queue --status=0`

To watch a list of urls being processed: `watch -n 1 bin/prerender prerender:display:queue --status=1`
